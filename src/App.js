import React, { useState } from 'react';
import './App.css';
import { Switch, Route, Link, useParams } from 'react-router-dom';


const Home = () => {
  return (
    <>
    <h1>Accueil</h1>
    <img src='./img/monImg.jpg' alt='groupe'></img>
    </>
  )
}

const Child =() => {
  let {id} = useParams()
return (
  <>
  <h3>Produit : {id}</h3>
  </>
)
}

const Products =() =>{
  return (
    <>
    <h2>Produits</h2>
    <ul>
          <li>
            <Link to='/products/bag'>Bag</Link>
          </li>
          <li>
            <Link to="/products/portable">Portable</Link>
          </li>
          <li>
            <Link to="/products/batteries">Batteries</Link>
          </li>          
        </ul>
        <Switch>
          <Route path="/products/:id" children={<Child />} />
        </Switch>
    </>
  )
}

const Contact = () => {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log (`${name}, ${email}`)
}
  return (
    <>
    <h2>Contact</h2>
    <form onSubmit={handleSubmit}>
      <label htmlFor='name'>Nom :</label>
      <input type='text' 
      placeholder='Nom' 
      value={name}
      onChange={e => setName(e.target.value)}
      />
       <label htmlFor='email'> Email :</label>
      <input type='text' 
      placeholder='name@email.fr' 
      value={email}
      onChange={e => setEmail(e.target.value)}
      />
      <input type="submit" value="Submit" />
    </form>
    </>
  )
}

const App = () => {
  return (
    <div className="App">
      <Link to='/'>Home</Link> <Link to='/products'>Produits</Link> <Link to='/contact'>Contact</Link>
      <Switch>
        <Route exact path='/'><Home /></Route>
        <Route path='/products'><Products /></Route>
        <Route path='/contact'>
          <Contact />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
